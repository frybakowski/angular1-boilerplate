import SampleController from './sample.controller';

import template from './sample.html';

module.exports = {
  template: template,
  controller: ['$state', SampleController],
  bindings: {
    data: '<'
  }
};
