export default class SampleController {
    constructor($state) {
        this.$state = $state;
    }

    onMoreClick() {
        this.$state.go('more')
    }
}