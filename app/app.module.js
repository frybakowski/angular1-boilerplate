import angular from 'angular'
import uirouter from 'angular-ui-router'

import routes from './app.routes'
import main from './modules/main'
import more from './modules/more'

angular.module('app', [uirouter, main, more])
  .config(routes);
