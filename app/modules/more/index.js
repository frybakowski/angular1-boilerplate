import angular from 'angular'
import uirouter from 'angular-ui-router'

import routes from './more.routes.js'
import detailsComponent from '../../components/details/details.component'

export default angular.module('app.more', [uirouter])
  .config(routes)
  .component('detailsComponent', detailsComponent)
  .name
