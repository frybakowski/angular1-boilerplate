import template from './more.html';

routes.$inject = ['$stateProvider'];
export default function routes($stateProvider) {
    $stateProvider
        .state('more', {
            url: '/more',
            template: template
        });
}
