import angular from 'angular'
import uirouter from 'angular-ui-router'

import routes from './main.routes.js'
import sampleComponent from '../../components/sample/sample.component'
import MockApiService from '../../services/mock-api.service'

export default angular.module('app.main', [uirouter])
  .config(routes)
  .component('sampleComponent', sampleComponent)
  .service('MockApiService', MockApiService)
  .name
