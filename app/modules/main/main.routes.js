import template from './main.html';

routes.$inject = ['$stateProvider'];
export default function routes($stateProvider) {
    $stateProvider
        .state('main', {
            url: '/',
            template: template,
            resolve: {
                mockData: ['MockApiService', (MockApiService) => {
                    return MockApiService.getMockData()
                }]
            }
        });
}
