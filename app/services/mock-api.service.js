export default class MockApiService {
    constructor($http, $q) {
        this.$http = $http;
        this.$q = $q
    }

    getMockData() {
        return this.$q((resolve) => {
            setTimeout(() => {
                resolve("This is components based Angular 1 application. ");
            }, 1000);
        });
    }

}
