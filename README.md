# ABOUT
Sample Angular application demonstrating structure.

Main features:<br />
**Component based architecture:** Currently frontend frameworks base on component structure.
This approach gives us reusability of our components independently from
the rest of application structure and logic. It is also big advantage in terms of application scalability.<br />
**Flat structure:** Flat structure of modules, components, services etc. makes application more readable.<br />
**ES6:** Gives breaking changes in JavaScript syntax and new possibilities. JavaScript more OOP friendly.<br />
**UI Router:** The most powerful and useful tool for managing Angular application routing.<br />
**Webpack:** Dependencies manager<br />

# RUN
npm install<br />
npm run start<br />